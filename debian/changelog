globus-openssl-module (5.2-3) unstable; urgency=medium

  * Enable bind-now hardening

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 06 Mar 2024 18:31:04 +0100

globus-openssl-module (5.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062166

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 18:35:00 +0000

globus-openssl-module (5.2-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:32:19 +0200

globus-openssl-module (5.2-1) unstable; urgency=medium

  * Minor fixes to makefiles
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 13:34:22 +0100

globus-openssl-module (5.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:47 +0200

globus-openssl-module (5.1-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:59 +0100

globus-openssl-module (5.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:32 +0200

globus-openssl-module (4.8-2) unstable; urgency=medium

  * Migrate to dbgsym packages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:36 +0200

globus-openssl-module (4.8-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 08 Nov 2016 22:03:08 +0100

globus-openssl-module (4.7-1) unstable; urgency=medium

  * GT6 update: Updates for OpenSSL 1.1.0 (Closes: #828324)
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 01 Sep 2016 09:29:52 +0200

globus-openssl-module (4.6-5) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 16:29:44 +0100

globus-openssl-module (4.6-4) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Fixes lintian man page warnings)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 15:08:29 +0200

globus-openssl-module (4.6-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 23:30:39 +0100

globus-openssl-module (4.6-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 17:18:48 +0100

globus-openssl-module (4.6-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-openssl-module-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 12:41:00 +0100

globus-openssl-module (4.5-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Drop -progs binary package - merged with globus-gsi-cert-utils-progs

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 10:06:46 +0200

globus-openssl-module (3.3-2) unstable; urgency=low

  * Remove junk man page

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 05 Dec 2013 22:19:27 +0100

globus-openssl-module (3.3-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 09 Nov 2013 19:36:17 +0100

globus-openssl-module (3.2-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 11:59:43 +0200

globus-openssl-module (3.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patch globus-openssl-module-sbin.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 28 Apr 2012 18:58:11 +0200

globus-openssl-module (3.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 16:16:52 +0100

globus-openssl-module (3.1-1) unstable; urgency=low

  * Update to Globus Toolkit version 5.2.0
  * Drop patch globus-openssl-module-mingw.patch (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 25 Dec 2011 10:14:19 +0100

globus-openssl-module (1.3-3) unstable; urgency=low

  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 07 Jun 2011 01:24:32 +0200

globus-openssl-module (1.3-2) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616244)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 05:09:16 +0200

globus-openssl-module (1.3-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.2
  * Drop patch globus-openssl-module-oid.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 19 Jul 2010 05:21:32 +0200

globus-openssl-module (1.2-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583056)
  * Fix OID registration pollution

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-openssl-module (1.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.1
  * Drop patch globus-openssl-module-sslinit.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 15 Apr 2010 10:04:56 +0200

globus-openssl-module (1.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Jan 2010 12:51:13 +0100

globus-openssl-module (0.6-4) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 11:37:11 +0200

globus-openssl-module (0.6-3) unstable; urgency=low

  * Initial release (Closes: #514466).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:34 +0200

globus-openssl-module (0.6-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-openssl-module (0.6-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 09:08:41 +0100
